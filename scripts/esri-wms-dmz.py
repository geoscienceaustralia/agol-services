#Written by: Daniel McIlroy
#Updated by: Daniel McIlroy --u81414
#Updated on: 2019-08-14

from arcgis.gis import GIS
from requests import post
from requests.exceptions import HTTPError
from json import loads
import sys, requests, time

def make_public(item):
    # Take item, make it public and add to group
    # Group: GA Web Services (WMS) - PROD
    groupID = "0be10905335e4ee78c367e5f807bf3ba"

    # Make public and add to group
    item.share(everyone=True, groups=groupID)


def agol_item_search(url, wmsURL):
    # Take url and search for it in AGOL
    username = sys.argv[1]
    password = sys.argv[2]
    gis = GIS("https://geoscience-au.maps.arcgis.com", username, password)

    # Make JSON request to get map title
    websvc_request = requests.get(url+"?f=json")
    title = websvc_request.json()["mapName"]

    # Use map title to search AGOL in the username account
    item = gis.content.search(query="title:"+ title + " AND owner: " + username, item_type="WMS", sort_field='title', sort_order='asc' )

    # If not present, upload
    if len(item) == 0:
        print("Not found, uploading to AGOL")
        register_esri_wms(url, wmsURL)
    else:
        print("Item is in AGOL")


def register_esri_wms(url, wmsURL):
    # Pull required parameters for AGOL upload from rest end point
    websvc_request = requests.get(url+"?f=json")
    title = websvc_request.json()["mapName"]
    docInfo = websvc_request.json()["documentInfo"]
    keywords = docInfo["Keywords"]
    descrip = docInfo["Comments"]
    copyInfo = websvc_request.json()["copyrightText"]
    extent = websvc_request.json()["fullExtent"]
    xmin = extent["xmin"]
    ymin = extent["ymin"]
    xmax = extent["xmax"]
    ymax = extent["ymax"]
    fullExtent = str(xmin) + "," + str(ymin) + "," + str(xmax) + "," + str(ymax)

    # AGOL Username and Password
    username = sys.argv[1]
    password = sys.argv[2]

    # Thumbnails from the DMZ are inconsistent, and break the script if not present
    #thumbnail = url + "/info/thumbnail/thumbnail.png"

    # Check if service is cached and set typeKeywords accordingly
    if websvc_request.json()["singleFusedMapCache"] == True:
        tKeywords = "Data, Service, Web Map Service, OGC ,Tiled ,Multilayer"
        print("Service is Cached")
    else:
        tKeywords = "Data, Service, Web Map Service, OGC, Multilayer"
        print("Service is Not Cached")

    # List of parameters to be passed into AGOL for the service
    params = {"type": "WMS",
              "typeKeywords": tKeywords,
              "url": wmsURL,
              "title": title,
              "tags": keywords + ", DMZ Service",
              "description": descrip,
              "licenceInfo": copyInfo,
              "accessInformation": copyInfo,
              "extent": fullExtent}

    # Add service via ArcGIS API for Python
    params['title'] = title
    gis = GIS("https://geoscience-au.maps.arcgis.com", username, password)
    gis.content.add(params, folder="Esri DMZ")
    print("ESRI WMS AGOL UPLOAD SUCCESSFUL")

    # Wait for the service to fully register in AGOL
    time.sleep(2)

    # Identify new item in AGOL
    item = gis.content.search(query="title:"+ title + " AND owner: " + username, item_type="WMS", sort_field='title', sort_order='asc' )
    itemList = item[0]

    # Use make_public function
    make_public(itemList)
    print("ESRI WMS ITEM MADE PUBLIC")
    return


def get_services():
    # Wait 1sec to start next services
    time.sleep(2)

    # Identify individual services from rest endpoint
    rest_endpoint = "http://services.ga.gov.au/site_{}/rest/services".format(sys.argv[3])  #Set our rest enpoint to use the stack number
    try:
        rest_endpoint_response = requests.get(rest_endpoint+"?f=json")  # Get the rest page contents using a json format
    #Error alerts in case a rest endpoint returns a 404 or other error
    except HTTPError as http_err:
        print('HTTP error occurred: ' + str(http_err))
        return
    except Exception as err:
        print('Other error occurred:' + str(err))
        return

    #Loop through the json list of services and build the production web services
    for service in rest_endpoint_response.json()["services"]:
        url = ("https://services.ga.gov.au/gis/rest/services/{}/{}".format(service["name"],service["type"]))
        wmsURL = ("https://services.ga.gov.au/gis/services/{}/{}/WMSServer".format(service["name"],service["type"]))
        print("*************************************************************")
        print(url)
        websvc_request = requests.get(url+"?f=json")

        # Use Map Server services only
        if "MapServer" in service["type"]:
            wmsTest = websvc_request.json()["supportedExtensions"]
            if "WMS" in wmsTest:
                agol_item_search(url, wmsURL)
            else:
                print("NO WMS PRESENT")

        print("COMPLETE")
        print("*************************************************************")


if __name__ == '__main__':
    get_services()
