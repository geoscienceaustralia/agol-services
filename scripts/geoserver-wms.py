#Written by: Ben Turrell & Daniel McIlroy
#Updated by: Daniel McIlroy --u81414
#Updated on: 2019-08-14

from arcgis.gis import GIS
from requests import post
from requests.exceptions import HTTPError
from json import loads
from xml.dom import minidom
import sys, requests, time, boto3

def make_public(item):
    # Take item, make it public and add to group
    # Group: GA Web Services (WMS) - PROD
    groupID = "0be10905335e4ee78c367e5f807bf3ba"

    # Make public and add to group
    item.share(everyone=True, groups=groupID)


def agol_item_search(gsvrURL):
    # Take url and search for it in AGOL
    username = sys.argv[1]
    password = sys.argv[2]
    gis = GIS("https://geoscience-au.maps.arcgis.com", username, password)

    # Make JSON request to get map title
    xml_url = gsvrURL  + '?service=WMS&request=GetCapabilities&f=json'
    xml_str = requests.get(xml_url)
    xmldoc = minidom.parseString(xml_str.content)

    Title = xmldoc.getElementsByTagName('Title')
    title = (Title[0].firstChild.nodeValue)

    # Use map title to search AGOL in the username account
    item = gis.content.search(query="title:"+ title + " AND owner: " + username, item_type="WMS", sort_field='title', sort_order='asc' )

    # If not present, upload
    if len(item) == 0:
        print("Not found, uploading to AGOL")
        register_gsvr_wms(gsvrURL)
    else:
        print("Item is in AGOL")


def register_gsvr_wms(url):
    # Pull required parameters for AGOL upload from rest end point
    xml_url = url  + '?service=WMS&request=GetCapabilities&f=json'
    xml_str = requests.get(xml_url)
    xmldoc = minidom.parseString(xml_str.content)

    Title = xmldoc.getElementsByTagName('Title')
    title = (Title[0].firstChild.nodeValue)
    Abstract = xmldoc.getElementsByTagName('Abstract')
    descrip = (Abstract[0].firstChild.nodeValue)
    AccessConst = xmldoc.getElementsByTagName('AccessConstraints')
    copyInfo = (AccessConst[0].firstChild.nodeValue)

    # Pull all keywords from each layer. Append Geoserver tag
    keywordCSV = []
    KeywordList = xmldoc.getElementsByTagName('Keyword')
    for keyword in KeywordList:
        keywordCSV.append(keyword.firstChild.nodeValue)
    keywordCSV = list(set(keywordCSV))
    keywordCSV.append("Geoserver")

    # AGOL Username and Password
    username = sys.argv[1]
    password = sys.argv[2]

    # Defined Geoserver WMS typeKeywords
    tKeywords = "Data, Service, Web Map Service, OGC, Multilayer, Geoserver"

    # List of parameters to be passed into AGOL for the service
    params = {"type": "WMS",
              "typeKeywords": tKeywords,
              "url": url,
              "title": title,
              "tags": keywordCSV,
              "description": descrip,
              "licenceInfo": copyInfo,
              "accessInformation": copyInfo}

    # Add service via ArcGIS API for Python
    params['title'] = title
    gis = GIS("https://geoscience-au.maps.arcgis.com", username, password)
    gis.content.add(params, folder="Geoserver WMS")
    print("GEOSERVER WMS AGOL UPLOAD SUCCESSFUL")

    # Wait for the service to fully register in AGOL
    time.sleep(2)

    # Identify new item in AGOL
    item = gis.content.search(query="title:"+ title + " AND owner: " + username, item_type="WMS", sort_field='title', sort_order='asc' )
    itemList = item[0]

    # Use make_public function
    make_public(itemList)
    print("GEOSERVER WMS ITEM MADE PUBLIC")
    return

def geosvr_getsvc():
    # Identify individual Geoserver services from s3 bucket
    client = boto3.client('s3')
    paginator = client.get_paginator('list_objects')
    result = paginator.paginate(Bucket='geoserver-webservices', Delimiter='/')

    for prefix in result.search('CommonPrefixes'):
        service_name = prefix.get('Prefix').replace("/","")
        gsvrURL = ("http://services.ga.gov.au/gis/{}/wms".format(service_name))
        testURL = gsvrURL  + '?service=WMS&request=GetCapabilities&f=json'
        print("*************************************************************")
        print(gsvrURL)

        # If valid endpoint, search AGOL for existance
        if (requests.get(testURL).status_code == 200):
            agol_item_search(gsvrURL)

        print("COMPLETE")
        print("*************************************************************")


if __name__ == '__main__':
    geosvr_getsvc()
